# =============== Users ===================================== #
USERS="$(dirname $(dirname ${PROJECT}))/user-wallets"         #
ALICE="${USERS}/alice.pem"                                    # 
BOB="${USERS}/bob.pem"                                        #
CAROL="${USERS}/carol.pem"                                    #
MIKE="${USERS}/mike.pem"                                      #
# =========================================================== #

# ======== Testnet settings ================= #
PROXY="https://testnet-gateway.elrond.com"    #
CHAIN="T"                                     #
# =========================================== #

# ======== Token identifiers ================ #
BITCOIN_TOKEN_IDENTIFIER="MYBTC-ac2836"       #
USD_TOKEN_IDENTIFIER="MYUSD-df628c"           #  
# =========================================== #

# ========= Erdpy helper variables ============================= #
CONTRACT_ADDRESS_KEY="order-book-address"                        #
CONTRACT_ADDRESS_EXPRESSION="data['emitted_tx']['address']"      #
DEPLOYMENT_TRANSACTION_KEY="order-book-tx"                       # 
DEPLOYMENT_TRANSACTION_EXPRESSION="data['emitted_tx']['hash']"   #
INTERACTION_FILE="order-book.json"                               #  
# ============================================================== #

# ========= Global variables for contract ===================================== #
CONTRACT_ADDRESS=$(erdpy data load --key=${CONTRACT_ADDRESS_KEY})               #  
DEPLOY_TRANSACTION=$(erdpy data load --key=${DEPLOYMENT_TRANSACTION_KEY})       #  
DEPLOY_GAS="110948100"                                                          #  
# ============================================================================= #

deploy_to_testnet() {
    echo
    echo "#======================================= #"
    echo "|    DEPLOYING ORDER BOOK CONTRACT       |"
    echo "#======================================= #"    
    echo 
    
    echo "Owner: ${ALICE}"
    echo "Token1: ${USD_TOKEN_IDENTIFIER}"
    echo "TOKEN2: ${BITCOIN_TOKEN_IDENTIFIER}"
    
    local first_token_hex;
    local second_token_hex;

    first_token_hex=0x$(echo -n ${USD_TOKEN_IDENTIFIER} | xxd -p -u | tr -d '\n')
    second_token_hex=0x$(echo -n ${BITCOIN_TOKEN_IDENTIFIER} | xxd -p -u | tr -d '\n')

    erdpy --verbose contract deploy --project=${PROJECT} \
                                    --recall-nonce \
                                    --pem=${ALICE} \
                                    --gas-limit=${DEPLOY_GAS} \
                                    --arguments ${first_token_hex} ${second_token_hex} \
                                    --proxy=${PROXY} \
                                    --chain=${CHAIN} \
                                    --metadata-payable \
                                    --outfile=${INTERACTION_FILE} \
                                    --send || return

    DEPLOY_TRANSACTION=$(erdpy data parse --file=${INTERACTION_FILE} --expression="data['emitted_tx']['hash']")
    CONTRACT_ADDRESS=$(erdpy data parse --file=${INTERACTION_FILE} --expression="data['emitted_tx']['address']")

    erdpy data store --key=${CONTRACT_ADDRESS_KEY} --value=${CONTRACT_ADDRESS}
    erdpy data store --key=${DEPLOYMENT_TRANSACTION_KEY} --value=${DEPLOY_TRANSACTION}
    
    echo
    echo "#===================================================================================================== #"
    echo "|   Contract Address: ${CONTRACT_ADDRESS}                  |"
    echo "|------------------------------------------------------------------------------------------------------|"
    echo "|    Deployment Transaction: ${DEPLOY_TRANSACTION}         |"
    echo "#===================================================================================================== #"    
    echo

}


bob_creates_a_buy_order() {

    local endpoint_name;
    local endpoint_arguments;

    endpoint_name="createBuyOrder"
    endpoint_arguments="
        0x$(echo -n $ 690000000000 | xxd -p -u | tr -d '\n'),
        0x00,
        0x01,
        0x$(echo -n $ 1000000 | xxd -p -u | tr -d '\n'),
        0x$(echo -n $ 500000 | xxd -p -u | tr -d '\n'),
    "

    erdpy --verbose contract call ${CONTRACT_ADDRESS} --pem=${BOB} \
                                                      --proxy=${PROXY} \
                                                      --chain=${CHAIN} \
                                                      --recall-nonce \
                                                      --gas-limit=${DEPLOY_GAS} \
                                                      --function=${endpoint_name} \
                                                      --arguments ${endpoint_arguments} \
                                                      --value=6900000000 \
                                                      --send || return

}

# mike_creates_a_sell_order() {

# }

# alice_matches_the_orders() {

# }

