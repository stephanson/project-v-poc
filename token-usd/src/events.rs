elrond_wasm::imports!();
elrond_wasm::derive_imports!();

#[elrond_wasm::module]
pub trait EventsModule {

    #[event("issue-started")]
    fn issue_started_event(
        &self,
        #[indexed] caller: &Address,
        #[indexed] token_ticker: &[u8],
        initial_supply: &Self::BigUint,
    );

    #[event("issue-success")]
    fn issue_success_event(
        &self,
        #[indexed] caller: &Address,
        #[indexed] token_identifier: &TokenIdentifier,
        initial_supply: &Self::BigUint,
    );

    #[event("issue-failure")]
    fn issue_failure_event(&self, #[indexed] caller: &Address, message: &[u8]);

    #[event("mint-started")]
    fn mint_started_event(&self, #[indexed] caller: &Address, amount: &Self::BigUint);

    #[event("mint-success")]
    fn mint_success_event(&self, #[indexed] caller: &Address);

    #[event("mint-failure")]
    fn mint_failure_event(&self, #[indexed] caller: &Address, message: &[u8]);

}