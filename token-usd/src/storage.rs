elrond_wasm::imports!();
elrond_wasm::derive_imports!();

#[elrond_wasm::module]
pub trait StorageModule {

    #[view(getUsdTokenIdentifier)]
    #[storage_mapper("usd_token_id")]
    fn usd_token_id(&self) -> SingleValueMapper<Self::Storage, TokenIdentifier>;

    #[view(getUnusedUsdToken)]
    #[storage_mapper("unused_usd_token")]
    fn unused_usd_token(&self) -> SingleValueMapper<Self::Storage, Self::BigUint>;

}