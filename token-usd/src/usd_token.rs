#![no_std]

elrond_wasm::imports!();
elrond_wasm::derive_imports!();

const DECIMALS: usize = 18;

mod storage;
mod events;

#[elrond_wasm::contract]
pub trait UsdToken: storage::StorageModule + events::EventsModule {

    #[init]
    fn init(&self) {}

    #[only_owner]
    #[payable("*")]
    #[endpoint(issueUsdToken)]
    fn issue_usd_token(
        &self,
        token_display_name: BoxedBytes,
        token_ticker: BoxedBytes,
        initial_supply: Self::BigUint,
        #[payment] issue_cost: Self::BigUint
    ) -> SCResult<AsyncCall<Self::SendApi>> {

        require!(
            self.usd_token_id().is_empty(),
            "Usd token was already issued"
        );

        let caller = self.blockchain().get_caller();
        self.issue_started_event(&caller, token_ticker.as_slice(), &initial_supply);

        Ok(ESDTSystemSmartContractProxy::new_proxy_obj(self.send())
            .issue_fungible(
                issue_cost,
                &token_display_name,
                &token_ticker,
                &initial_supply,
                FungibleTokenProperties {
                    num_decimals: DECIMALS,
                    can_freeze: true,
                    can_wipe: false,
                    can_pause: false,
                    can_mint: true,
                    can_burn: true,
                    can_change_owner: true,
                    can_upgrade: true,
                    can_add_special_roles: false,
                },
            )
            .async_call()
            .with_callback(self.callbacks().esdt_issue_callback(&caller)))
    }

    #[callback]
    fn esdt_issue_callback(
        &self,
        caller: &Address,
        #[payment_token] token_identifier: TokenIdentifier,
        #[payment] returned_tokens: Self::BigUint,
        #[call_result] result: AsyncCallResult<()>,
    ) {
        // callback is called with ESDTTransfer of the newly issued token, with the amount requested,
        // so we can get the token identifier and amount from the call data
        match result {
            AsyncCallResult::Ok(()) => {
                self.issue_success_event(caller, &token_identifier, &returned_tokens);
                self.unused_usd_token().set(&returned_tokens);
                self.usd_token_id().set(&token_identifier);
            },
            AsyncCallResult::Err(message) => {
                self.issue_failure_event(caller, message.err_msg.as_slice());            
            },
        }
    }

    #[only_owner]
    #[endpoint(mintUsdToken)]
    fn mint_usd_token(&self, amount: Self::BigUint) -> SCResult<AsyncCall<Self::SendApi>> {
        require!(
            !self.usd_token_id().is_empty(),
            "USD Token was not issued yet"
        );

        let usd_token_id = self.usd_token_id().get();
        let caller = self.blockchain().get_caller();
        self.mint_started_event(&caller, &amount);

        Ok(ESDTSystemSmartContractProxy::new_proxy_obj(self.send())
            .mint(&usd_token_id, &amount)
            .async_call()
            .with_callback(self.callbacks().esdt_mint_callback(&caller, &amount)))
    }

    #[callback]
    fn esdt_mint_callback(
        &self,
        caller: &Address,
        amount: &Self::BigUint,
        #[call_result] result: AsyncCallResult<()>,
    ) {
        match result {
            AsyncCallResult::Ok(()) => {
                self.mint_success_event(caller);
                self.unused_usd_token()
                    .update(|unused_usd_token| *unused_usd_token += amount);
            },
            AsyncCallResult::Err(message) => {
                self.mint_failure_event(caller, message.err_msg.as_slice());
            },
        }
    }

    #[payable("*")]
    #[endpoint(sendFiftyBucks)]
    fn send_fifty_bucks(
        &self,
        #[payment_amount] amount: Self::BigUint
    ) -> SCResult<()> {

        let token_id = self.usd_token_id().get();        
        let caller = self.blockchain().get_caller();

        self.send().direct(
            &caller,
            &token_id,
            0,
            &amount,
            &[],
        );

        Ok(())
    }
}
