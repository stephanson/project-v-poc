elrond_wasm::imports!();
elrond_wasm::derive_imports!();

#[elrond_wasm::module]
pub trait StorageModule {

    #[view(getBitcoinTokenIdentifier)]
    #[storage_mapper("bitcoin_token_id")]
    fn bitcoin_token_id(&self) -> SingleValueMapper<Self::Storage, TokenIdentifier>;

    #[view(getUnusedBitcoinToken)]
    #[storage_mapper("unused_bitcoin_token")]
    fn unused_bitcoin_token(&self) -> SingleValueMapper<Self::Storage, Self::BigUint>;

}