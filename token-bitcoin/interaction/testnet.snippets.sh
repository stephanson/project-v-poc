# =============== Users ==================== #
USERS="$(dirname ${PROJECT})/user-wallets"   #
ALICE="${USERS}/alice.pem"                   # 
BOB="${USERS}/bob.pem"                       #
CAROL="${USERS}/carol.pem"                   #
MIKE="${USERS}/mike.pem"                     #
# ========================================== #

# ======== Testnet settings ================= #
PROXY="https://testnet-gateway.elrond.com"    #
CHAIN="T"                                     #
# =========================================== #

# ========= Erdpy helper variables ============================= #
CONTRACT_ADDRESS_KEY="bitcoin-address"             #
CONTRACT_ADDRESS_EXPRESSION="data['emitted_tx']['address']"      #
DEPLOYMENT_TRANSACTION_KEY="bitcoin-tx"            # 
DEPLOYMENT_TRANSACTION_EXPRESSION="data['emitted_tx']['hash']"   #
INTERACTION_FILE="bitcoin.json"                    #  
# ============================================================== #

# ============= Global variables for contract  ======================================== #
CONTRACT_ADDRESS=$(erdpy data load --key=${CONTRACT_ADDRESS_KEY})                       #  
DEPLOY_TRANSACTION=$(erdpy data load --key=${DEPLOYMENT_TRANSACTION_KEY})               #  
DEPLOY_GAS="80000000"                                                                   #  
# ===================================================================================== #

HARDCODED_CONTRACT_ADDRESS="erd1qqqqqqqqqqqqqpgqhkujg78nk7tkd9gfx8gw9vmrqvmrh9h4d8ss0cjgat"


deploy_to_testnet() {
    echo "#======================================= #"
    echo "|    DEPLOYING BITCOIN TOKEN CONTRACT    #"
    echo "#======================================= #"    
    
    echo
    echo "Owner: ${ALICE}"
    echo 

    erdpy --verbose contract deploy --project=${PROJECT} \
                                    --recall-nonce \
                                    --pem=${ALICE} \
                                    --gas-limit=${DEPLOY_GAS} \
                                    --proxy=${PROXY} \
                                    --chain=${CHAIN} \
                                    --metadata-payable \
                                    --outfile=${INTERACTION_FILE} \
                                    --send || return

    DEPLOY_TRANSACTION=$(erdpy data parse --file=${INTERACTION_FILE} --expression=${DEPLOYMENT_TRANSACTION_EXPRESSION})
    CONTRACT_ADDRESS=$(erdpy data parse --file=${INTERACTION_FILE} --expression=${CONTRACT_ADDRESS_EXPRESSION})

    erdpy data store --key=${CONTRACT_ADDRESS_KEY} --value=${CONTRACT_ADDRESS}
    erdpy data store --key=${DEPLOYMENT_TRANSACTION_KEY} --value=${DEPLOY_TRANSACTION}
    

    echo
    echo "#=====================================================================================================#"
    echo "|   Contract Address: ${CONTRACT_ADDRESS}                  |"
    echo "|-----------------------------------------------------------------------------------------------------|"
    echo "|    Deployment Transaction: ${DEPLOY_TRANSACTION}         |"
    echo "#=====================================================================================================#"    
    echo 
}

verify_bitcoin_token_deployment() {
    erdpy tx get --hash=$DEPLOY_TRANSACTION --omit-fields="['data', 'signature']" --proxy=${PROXY}    
    erdpy account get --address=$CONTRACT_ADDRESS --omit-fields="['code']" --proxy=${PROXY}    
}

issue_bitcoin_token() {
    echo 
    echo "#============================================ #"
    echo "# ISSUE BITCOIN TOKEN ON TESTNET              #"  
    echo "#============================================ #"
    echo

    local token_name;
    local token_ticker;
    local supply;
    local issue_cost;
    local contract_method;
    local method_arguments;

    token_name=0x$(echo -n MyBitcoin | xxd -p -u | tr -d '\n')
    token_ticker=0x$(echo -n MYBTC | xxd -p -u | tr -d '\n')
    supply=0x$(echo -n 50000000000000 | xxd -p -u | tr -d '\n')
    issue_cost=0x$(echo -n 50000000000000000 | xxd -p -u | tr -d '\n')
    
    contract_method="issueBitcoinToken"    

    echo "Function: ${contract_method}"
    echo "Args (hex): ${method_arguments}"

    echo "# ================================================================================= #"
    echo "#                             Transaction details                                   #"
    echo "# ================================================================================= #"
    echo 

    erdpy --verbose contract call ${BITCOIN_TOKEN_CONTRACT_ADDRESS} --recall-nonce \
                                                                    --pem=${ALICE} \
                                                                    --function=${contract_method} \
                                                                    --arguments ${token_name} ${token_ticker} ${supply} \
                                                                    --proxy=${PROXY} \
                                                                    --chain=${CHAIN} \
                                                                    --value=50000000000000000 \
                                                                    --gas-limit=${DEPLOY_GAS} \
                                                                    --send || return

}

verify_token_id_set() {    
    erdpy --verbose contract query erd1qqqqqqqqqqqqqpgqhkujg78nk7tkd9gfx8gw9vmrqvmrh9h4d8ss0cjgat --function="getBitcoinTokenIdentifier" --proxy=${PROXY}
}


upgrade_contract() {
    echo "#======================================= #"
    echo "|    UPGRADING BITCOIN TOKEN CONTRACT    #"
    echo "#======================================= #"    
    
    echo
    echo "Owner: ${ALICE}"
    echo 

    local contract_address;
    contract_address=${HARDCODED_CONTRACT_ADDRESS}

    erdpy --verbose contract upgrade ${contract_address} --project=${PROJECT} \
                                                        --recall-nonce \
                                                        --pem=${ALICE} \
                                                        --gas-limit=${DEPLOY_GAS} \
                                                        --proxy=${PROXY} \
                                                        --chain=${CHAIN} \
                                                        --metadata-payable \
                                                        --outfile=${INTERACTION_FILE} \
                                                        --send || return

     

    echo
    echo "#=====================================================================================================#"
    echo "|   Contract Address: ${contract_address}                  |"   
    echo "#=====================================================================================================#"    
    echo 
}


send_bitcoin_to_bob() {
    echo
    echo "#=================================================== #"
    echo "              SENDING MYBTC to BOB                   #"
    echo "#=================================================== #"

    local contract_address;
    contract_address=$HARDCODED_CONTRACT_ADDRESS    

    erdpy --verbose contract call ${contract_address} --pem=${BOB} \
                                                      --recall-nonce \
                                                      --gas-limit=${DEPLOY_GAS} \
                                                      --function="sendFiftyBucks" \
                                                      --value=6900000000000000000 \
                                                      --proxy=${PROXY} \
                                                      --chain=${CHAIN} \
                                                      --send  

}