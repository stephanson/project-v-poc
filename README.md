
# Project-V POC




## Installation

1. Clone the repository
2. Install VS Code
3. Install the `Elrond IDE` VS Code extension (should install erdpy, otherwise install erdpy manually)
4. Install the `rust analyzer` VS Code extension and disable the `Rust` VS Code extension if it is enabled.
5. Run the build-all script
6. Right click the projects and run `Run contract snippets`

    


## Structure

Contracts for:
 - custom usd token (refered in this doc as token1)
 - custom bitcoin token (refered in this doc as token2)
 - token1 leverage pool
 - token2 leverage pool
 - orderbook

#### Interaction folders contain the shell scripts which use erdpy to either deploy, upgrade or call functions inside the contracts.
#### Mandos folders contain the json specifications of the test cases for the contracts.

Contracts are generally structured into: the main part, a storage module, a views module, an events module and modules for secondary/auxiliary behavior.
The main parts will generally contain the endpoints and the constructor used for deployment and might use functions from the other modules.


  