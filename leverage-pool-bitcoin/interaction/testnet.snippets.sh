# =============== Users ======================= #
USERS="$(dirname ${PROJECT})/user-wallets"      #
ALICE="${USERS}/alice.pem"                      # 
BOB="${USERS}/bob.pem"                          #
CAROL="${USERS}/carol.pem"                      #
MIKE="${USERS}/mike.pem"                        #
# ============================================= #

# ======== Testnet settings ================= #
PROXY="https://testnet-gateway.elrond.com"    #
CHAIN="T"                                     #
# =========================================== #

# ========= Erdpy helper variables ============================= #
CONTRACT_ADDRESS_KEY="bitcoin-leverage-pool-address"             #
CONTRACT_ADDRESS_EXPRESSION="data['emitted_tx']['address']"      #
DEPLOYMENT_TRANSACTION_KEY="bitcoin-leverage-pool-tx"            # 
DEPLOYMENT_TRANSACTION_EXPRESSION="data['emitted_tx']['hash']"   #
INTERACTION_FILE="bitcoin-leverage-pool.json"                    #  
# ============================================================== #

# ============= Global variables for contract  ======================================== #
CONTRACT_ADDRESS=$(erdpy data load --key=address-testnet)                                #  
DEPLOY_TRANSACTION=$(erdpy data load --key=deployTransaction-testnet)                   #  
DEPLOY_GAS="80000000"                                                                   #  
# ===================================================================================== #

# ============== Token identifiers ============ #
POOL_TOKEN_IDENTIFIER="MYBTC-ac2836"            #
# ============================================= #

deploy_to_testnet() {
        
    echo 
    echo "#========================================#"
    echo "|    DEPLOYING BITCOIN LEVERAGE POOL     |"
    echo "#========================================#"    
    echo
    
    local token_identifier;
    token_identifier=0x$(echo -n ${POOL_TOKEN_IDENTIFIER} | xxd -p -u | tr -d '\n')

    echo "Owner: ${ALICE}"
    echo "Token: ${POOL_TOKEN_IDENTIFIER}"
    echo "Token (hex): ${token_identifier}"    
    echo

    erdpy --verbose contract deploy \
          --project=${PROJECT} \
          --recall-nonce \
          --pem=${ALICE} \
          --gas-limit=${DEPLOY_GAS} \
          --arguments ${token_identifier} \
          --outfile=${INTERACTION_FILE} \
          --send \
          --proxy=${PROXY} \
          --chain=${CHAIN} || return

    DEPLOY_TRANSACTION=$(erdpy data parse --file=${INTERACTION_FILE} --expression=${DEPLOYMENT_TRANSACTION_EXPRESSION})
    CONTRACT_ADDRESS=$(erdpy data parse --file=${INTERACTION_FILE} --expression=${CONTRACT_ADDRESS_EXPRESSION})

    erdpy data store --key=${CONTRACT_ADDRESS_KEY} --value=${CONTRACT_ADDRESS}
    erdpy data store --key=${DEPLOYMENT_TRANSACTION_KEY} --value=${DEPLOY_TRANSACTION}

    echo
    echo "#==================================================================================================== #"
    echo "|   Contract Address: ${CONTRACT_ADDRESS}                  |"
    echo "|-----------------------------------------------------------------------------------------------------|"
    echo "|   Deployment Transaction: ${DEPLOY_TRANSACTION}          |"
    echo "#==================================================================================================== #"    
    echo 
}