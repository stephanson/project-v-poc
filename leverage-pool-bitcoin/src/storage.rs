elrond_wasm::imports!();

#[elrond_wasm::module]
pub trait BitcoinStorage {
    #[view(getOwner)]
    #[storage_mapper("owner")]
    fn pool_owner(&self) -> SingleValueMapper<Self::Storage, Address>;

    #[view(getPoolToken)]
    #[storage_mapper("poolToken")]
    fn pool_token(&self) -> SingleValueMapper<Self::Storage, TokenIdentifier>;

    #[view(getPoolTotalAmount)]
    #[storage_mapper("poolTotalAmount")]
    fn pool_total_amount(&self) -> SingleValueMapper<Self::Storage, Self::BigUint>;

    #[view(getPoolProviders)]
    #[storage_mapper("poolProviders")]
    fn pool_providers(&self, provider: &Address) -> SingleValueMapper<Self::Storage, Self::BigUint>;

    #[view(getPoolLeveragers)]
    #[storage_mapper("poolLeveragers")]
    fn pool_leveragers(&self, leverager: &Address) -> SingleValueMapper<Self::Storage, Self::BigUint>;
}