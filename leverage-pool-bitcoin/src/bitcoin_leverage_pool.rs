#![no_std]

elrond_wasm::imports!();
elrond_wasm::derive_imports!();

mod storage;
mod views;

#[elrond_wasm::contract]
pub trait BitcoinLeveragePool: storage::BitcoinStorage + views::BitcoinViews {

    #[init]
    fn init(&self, token_identifier: TokenIdentifier) -> SCResult<()> {
        
        require!(
            token_identifier.is_egld() || token_identifier.is_valid_esdt_identifier(),
            "Invalid token identifier set for pool."
        );
        
        let caller = self.blockchain().get_caller();
        self.pool_owner().set(&caller);   
        self.pool_token().set(&token_identifier);
        self.pool_total_amount().clear();

        Ok(())
    }

    // endpoints

    #[endpoint(addLiquidity)]
    #[payable("*")]
    fn add_liquidity(
        &self, 
        #[payment_amount] token_amount: Self::BigUint,
        #[payment_token] token_name: TokenIdentifier
    ) -> SCResult<()> {                  

        require!(
            self.pool_token().get() == token_name,
            "Token identifier does not match with the pool token identifier. (Expected Bitcoin)"
        );

        require!(
            token_amount > 0,
            "Token amount is zero."
        );

        let provider = self.blockchain().get_caller();

        self.pool_providers(&provider).update(|provider_amount| *provider_amount += &token_amount);
        self.pool_total_amount().update(|current_amount| *current_amount += &token_amount);        

        Ok(())
    }

    #[endpoint(leverageLiquidity)]
    #[payable("*")]    
    fn leverage(
        &self,
        #[payment_amount] token_amount: Self::BigUint,
        #[payment_token] token_name: TokenIdentifier
    ) -> SCResult<()> {
        
        require!(
            self.pool_token().get() == token_name,
            "Token identifier does not match with the pool token identifier. (Expected Bitcoin)"
        );

        require!(
            self.pool_total_amount().get() > token_amount,
            "Contract holds insufficient funds."
        );

        let caller = self.blockchain().get_caller();


        self.transfer_esdt(
            &caller,
            &token_name,
            &token_amount,
            b"Leverage transaction."
        );

        self.pool_total_amount().update(|current_amount| *current_amount -= &token_amount);
        self.pool_leveragers(&caller).set(&token_amount);

        Ok(())
        
    }

    #[endpoint(withdrawLiquidity)]
    fn withdraw_liquidity(&self) -> SCResult<()> {
        let caller = self.blockchain().get_caller();

        require!(
            self.is_a_liquidity_provider(&caller),
            "Contract caller can't withdraw funds from pool because he is not a provider."
        );

        let withdraw_amount = self.pool_providers(&caller).get();

        require!(
            withdraw_amount < self.pool_total_amount().get(),
            "Contract has insufficient funds to proceed with withdrawal."
        );

        self.transfer_esdt(
            &caller,
            &self.pool_token().get(),
            &withdraw_amount,
            b"Withdrawing funds from pool."
        );

        self.pool_total_amount().update(|total_amount| *total_amount -= withdraw_amount);
        self.pool_providers(&caller).clear();

        Ok(())
    }

    // private

    fn transfer_esdt(
        &self,
        to: &Address,
        token_id: &TokenIdentifier,
        amount: &Self::BigUint,
        data: &'static [u8]
    ) {
        self.send().direct(
            to,
            token_id,
            0,
            amount,
            self.data_or_empty_if_sc(to, data),
        );
    }
    
    
    fn data_or_empty_if_sc(&self, dest: &Address, data: &'static [u8]) -> &[u8] {
        if self.blockchain().is_smart_contract(dest) {
            &[]
        } else {
            data
        }
    }
}
