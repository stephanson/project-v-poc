elrond_wasm::imports!();

#[elrond_wasm::module]
pub trait BitcoinViews: crate::storage::BitcoinStorage {

    #[view(isPoolProvider)]
    fn is_a_liquidity_provider(&self, provider_address: &Address) -> bool {
        !self.pool_providers(&provider_address).is_empty()
    }
}