#!/bin/sh

set -e
interaction_files=$(find . -name "testnet.snippets.sh")
for interaction in $interaction_files
do
    echo ""
    (set -x; source $interaction && deploy_to_testnet)
done